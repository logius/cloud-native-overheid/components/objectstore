# objectstore
The code in this repository enables creation of Minio S3 'tenants' using the [Minio operator](https://gitlab.com/logius/cloud-native-overheid/components/minio-operator).

**Documentation:**

* CRD documentation: https://github.com/minio/operator/blob/master/docs/crd.adoc
* Example of CR deployment: https://github.com/minio/operator/blob/master/examples/kustomization/base/tenant.yaml
